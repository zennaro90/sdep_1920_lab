#include <stdio.h>
#include <stdlib.h>

#include "stack.h"

// Utility function to add an element data in the stack 
// insert at the beginning 
void push(int el) {
	stack[++headIdx] = el;
}

// Utility function to pop top  
// element from the stack 
int pop() {
	return stack[headIdx--];
}

// Utility function to return the stack size
int size() {
	return headIdx + 1;
}

// Utility function to check if the stack is empty or not 
int isEmpty() {
	return headIdx < 0;
}

// Utility function to return top element in a stack 
int top() {
	return stack[headIdx];
}


int main(int argc, char *argv[]) {

	push(1);
	push(2);
	push(3);
	push(4);
	push(5);
	

	int i = 0;
	while(!isEmpty()) {
		printf("Posizione %d: %d\n", i++, pop());
	}
	return 0;
}

