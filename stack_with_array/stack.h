static int stack[100];

static int headIdx = -1;


// Utility function to add an element data in the stack 
// insert at the beginning 
void push(int el);

// Utility function to pop top  
// element from the stack 
int pop();

// Utility function to return the stack size
int size();

// Utility function to check if the stack is empty or not 
int isEmpty();

// Utility function to return top element in a stack 
int top();

