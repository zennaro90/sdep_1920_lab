#include <stdio.h>
#include <stdlib.h>

#include "map.h"


int main(int argc, char *argv[]) {
	init_map();
	
	put(0, "Mela");
	put(1, "Pera");
	printf("Pos 0: %s\n", get(0) -> value);
	printf("Pos 1: %s\n", get(1) -> value);
	put(1, "Fragola");
	printf("Pos 1: %s\n", get(1) -> value);
	
	printf("----------------------\n");
	int *p_keys = keys();
	while(*p_keys != -1) {
		printf("%d\n", *p_keys);
		p_keys++;
	}
	
	printf("----------------------\n");
	char **p_values = values();
	while(*p_values != NULL) {
		printf("%s\n", *p_values);
		p_values++;
	}
	
	
	printf("----------------------\n");
	struct Pair ** p_entries = entries();
	while(*p_entries != NULL) {
		printf("%d --> %s\n", (*p_entries) -> key, (*p_entries) -> value);
		p_entries++;
	}
	
	return 0;
}
