#ifndef MAP_H
#define MAP_H
#define MAP_MAX_SIZE  100

struct Pair {
	int key;
	char *value;
};

struct Pair * bucket[MAP_MAX_SIZE];

int map_size = 0;

void init_map();

/*
* Restituisce il numero di entry di M
*/
int size();
/*
* Verifica se M � vuota
*/
int isEmpty();

/*
* Se M contiene una entry e che ha un a chiave ugale a k, allora restituisce il valore di e, altrimenti restituisce NULL
*/
struct Pair * get(int k);

/*
* Se M non contiene una entry con chiave ugale a k, allora aggiunge la entry (k, v) a M e restituisce NULL.
* Altrimenti, sostituisce con n il valore esistente della entry avente come chiave k e restotuisce il vecchio valore
*/
char * put(int key, char *value);
/*
* Cancella da M la entry avente chiave k e restituisce l corrispondente valore, se M non contiente tale entry, allora resituisce NULL
*/
char * remove_entry(int key);
/*
* Resistuisce una lista contente tutte le chiavi contenute in M
*/
int * keys();

/*
* Restituisce una collection contenente tutti i valore associati alle chiavi contenute in M
*/
char ** values();

/*
* Trasforma la mappa in una lista
*/
struct Pair ** entries();

#endif
