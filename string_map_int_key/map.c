#include <stdio.h>
#include <stdlib.h>
#include "map.h"

void init_map() {
	
}

/*
* Restituisce il numero di entry di M
*/
int size() {
	return map_size;
}
/*
* Verifica se M � vuota
*/
int isEmpty() {
	return map_size == 0;
}

/*
* Se M contiene una entry e che ha un a chiave ugale a k, allora restituisce il valore di e, altrimenti restituisce NULL
*/
struct Pair * get(int k) {
	return bucket[k];
}

/*
* Se M non contiene una entry con chiave ugale a k, allora aggiunge la entry (k, v) a M e restituisce NULL.
* Altrimenti, sostituisce con n il valore esistente della entry avente come chiave k e restotuisce il vecchio valore
*/
char * put(int key, char *value) {
	char *c = bucket[key] != NULL ? bucket[key]->value : NULL;
	if(bucket[key] == NULL) {
		bucket[key] = malloc(sizeof(struct Pair));
		map_size++;
	}
	bucket[key]->key = key;
	bucket[key]->value = value;
	return c;
}
/*
* Cancella da M la entry avente chiave k e restituisce l corrispondente valore, se M non contiente tale entry, allora resituisce NULL
*/
char * remove_entry(int key) {
	char *c = bucket[key] != NULL ? bucket[key]->value : NULL;
	if(bucket[key] != NULL) {
		free(bucket[key]);
		map_size--;
	}
	return c;
}
/*
* Resistuisce una lista contente tutte le chiavi contenute in M
*/
int * keys() {
	int *p_vector;
	p_vector = calloc((map_size + 1),  sizeof(int));
	
	struct Pair **p_bucket; 

	int i = 0;
	for(p_bucket = bucket; p_bucket < &bucket[MAP_MAX_SIZE]; p_bucket++){
		if(*p_bucket != NULL) p_vector[i++] = (*p_bucket)->key;
	}
	p_vector[i] = -1; //marco l\'ultimo elemento
	return p_vector;
}

/*
* Restituisce una collection contenente tutti i valore associati alle chiavi contenute in M
*/
char ** values() {
	char **p_vector;
	p_vector = calloc((map_size + 1),  sizeof(char *));
	
	struct Pair **p_bucket; 

	int i = 0;
	for(p_bucket = bucket; p_bucket < &bucket[MAP_MAX_SIZE]; p_bucket++){
		if(*p_bucket != NULL) p_vector[i++] = (*p_bucket)->value;
	}
	p_vector[i] = NULL; //marco l\'ultimo elemento
	return p_vector;
}

/*
* Trasforma la mappa in una lista
*/
struct Pair ** entries() {
	struct Pair **p_vector;
	p_vector = calloc((map_size + 1),  sizeof(char *));
	
	struct Pair **p_bucket; 

	int i = 0;
	for(p_bucket = bucket; p_bucket < &bucket[MAP_MAX_SIZE]; p_bucket++){
		if(*p_bucket != NULL) p_vector[i++] = *p_bucket;
	}
	p_vector[i] = NULL; //marco l\'ultimo elemento
	return p_vector;
}
