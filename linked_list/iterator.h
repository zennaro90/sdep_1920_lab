#ifndef LINKED_LIST_H
#include "linked-list.h"
#endif

#ifndef ITERATOR_H
#define ITERATOR_H

struct iterator {
	struct node *next;
};

/*
* inizializza l'iteratore
*/
struct iterator * init_iterator();
/*
* indica se l'iteratore ha ancora elementi da scorrere
*/
int i_has_next(struct iterator *i);
/*
* resistuisce la posizione successiva dell'iteratore
*/
struct position * i_next(struct iterator *i);
/*
* distrugge l'iteratore
*/
void dispose_iterator(struct iterator *i);

#endif
