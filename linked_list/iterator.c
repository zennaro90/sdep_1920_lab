#include <stdio.h>
#include <stdlib.h>
#include "linked-list.h"
#include "iterator.h"

/*
* inizializza l'iteratore
*/
struct iterator * init_iterator() {
	struct iterator *i;
	i = malloc(sizeof(*i));
	i -> next = header -> next;
	return i;
}
/*
* indica se l'iteratore ha ancora elementi da scorrere
*/
int i_has_next(struct iterator *i) {
	return i->next != NULL && i->next != tailer;
}
/*
* resistuisce la posizione successiva dell'iteratore
*/
struct position * i_next(struct iterator *i){
	struct node *t = i -> next;
	i->next = t -> next;
	return t->pos;
}

/*
* distrugge l'iteratore
*/
void dispose_iterator(struct iterator *i) {
	free(i);
}
