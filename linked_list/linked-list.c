#include <stdio.h>
#include <stdlib.h>
#include "linked-list.h"
void init() {
	header = malloc(sizeof(struct node));
	tailer = malloc(sizeof(struct node));
	
	header->prev = NULL;
	header->next = tailer;
	
	
	tailer->prev = header;
	tailer->next = NULL;
}

/*
* Restituisce il primo elemento di S;
* 
*/
struct position * first() {
	return header -> next -> pos;
}


/*
* Restituisce l'ultimo nodo della lista
*/
struct position * last() {
	return tailer -> prev -> pos;
}


/*
* Ritorna un nodo data la posizione
*/
struct node * get_node(struct position *p) {
	struct node * current_node = header -> next;
	while(current_node -> pos != p && current_node -> next != NULL) {
		current_node = current_node -> next;
	}
	
	
	return current_node;	
}

/*
* Restituisce il nodo precedente a un determinato nodo della lista
*/
struct position * prev(struct position *p) {
	struct node *n = get_node(p);
	
	if(n == tailer || n -> prev == header) return NULL;
	
	return n -> prev -> pos;
}


/*
* Restituisce il nodo precedente a un determinato nodo della lista
*/
struct position * next(struct position *p) {
	struct node *n = get_node(p);
	
	if(n == tailer || n -> next == tailer) return NULL;
	
	return n -> next -> pos;
}

/*
* Sostituisce l'elemento memorizzato nel nodo dato, restituendo il vecchio elemento
*/
int set(struct position *p, int e){
	struct node *n = get_node(p);
	
	if(n == header || n == tailer) return -1;
	
	int t = n -> pos -> element;
	n -> pos -> element = e;
	return t;
}

/*
* Inserisce un elemento in cima alla lista restutuendo la nuova posizione
*/
struct position * add_first(int e) {
	struct position *p = malloc(sizeof(struct position));
	struct node *n = malloc(sizeof(struct node));
	struct node *next = header -> next;
	p->element = e;
	n->pos = p;
	n->prev = header;
	n->next = next;
	
	next -> prev = n;
	
	header->next = n;
	return p;
}

/*
* Inserisce un elemento in cima alla lista restutuendo la nuova posizione
*/
struct position * add_last(int e) {
	struct position *p = malloc(sizeof(struct position));
	struct node *n = malloc(sizeof(struct node));
	struct node *prev = tailer->prev;
	
	p->element = e;
	n->pos = p;
	n->prev = prev;
	
	prev -> next = n;
	
	n->next = tailer;
	tailer->prev = n;
	return p;
}

/*
* Inseriesce un elemento prima della data posizione
*/
void add_before(struct position *p, int e) {
	struct node *n_pos = get_node(p);
	
	struct position *new_p = malloc(sizeof(struct position));
	struct node *n = malloc(sizeof(struct node));
	
	new_p -> element = e;
	n -> pos = new_p;
	n -> next = n_pos;
	n -> prev = n_pos -> prev;  
	
	n_pos -> prev = n;
}

/*
* Inseriesce un elemento prima della data posizione
*/
void add_after(struct position *p, int e) {
	struct node *n_pos = get_node(p);
	
	struct position *new_p = malloc(sizeof(struct position));
	struct node *n = malloc(sizeof(struct node));
	
	new_p -> element = e;
	n -> pos = new_p;
	n -> prev = n_pos;
	n -> next = n_pos -> next;  
	
	n_pos -> next = n;
}

/*
* Rimuove un elemento in una data posizione
*/
int remove_element(struct position *p) {
	struct node *n_pos = get_node(p);
	
	if(n_pos == header || n_pos == tailer) return -1;
	
	struct node *prev = n_pos -> prev;
	struct node *next = n_pos -> next;
	
	prev -> next = next;
	next -> prev = prev;
	int e = n_pos -> pos -> element;
	free(n_pos -> pos);
	free(n_pos);
	return e;
} 

/*
* Scorre tutti gli elementi della lista richiamando la callback per ogni elemento
*/
void ll_for_each(void (*callback)(struct position *p)) {
	struct node *n = header -> next;
	while(n != tailer && n != NULL) {
		callback(n -> pos);
		n = n -> next;
	}
}

