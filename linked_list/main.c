#include <stdio.h>
#include <stdlib.h>

#include "linked-list.h"
#include "iterator.h"

void print_el(struct position *p) {
	printf("el: %d\n", p -> element);
}

void print_with_iterator() {
	struct iterator * i = init_iterator();
	while(i_has_next(i)) {
		printf("el: %d\n", i_next(i) -> element);
	}
	dispose_iterator(i);
}

int main(void) {
	init();
	
	
    struct position *p1 = add_first(15);
    struct position *p2 = add_first(5);
    
    struct position *p3 = prev(p1);
    struct position *p4 = next(p2);
    
    
     
	printf("First : %d \n", first() -> element);
	printf("Last : %d \n", last() -> element);
    printf("El 1: %d \n", p3 -> element);
    printf("El 2: %d \n", p4 -> element);
    
    int old_element = set(p1, 10);
    printf("old: %d --> new: %d\n", old_element, last() -> element);
    
    printf("Print with for each\n");
    ll_for_each(print_el);
    
    printf("Print with iterator\n");
    print_with_iterator();

    return EXIT_SUCCESS;
}
