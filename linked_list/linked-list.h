#ifndef LINKED_LIST_H
#define LINKED_LIST_H

struct position {
	int element;
};
struct node {
    struct position * pos;
    struct node * next;
    struct node * prev;
} node_t;

struct node *header;
struct node *tailer;


/*
* Restituisce il primo elemento di S;
* 
*/
struct position * first();

/*
* Restituisce l'ultimo nodo della lista
*/
struct position * last();

/*
* Restituisce il nodo precedente a un determinato nodo della lista
*/
struct position * prev(struct position *p);

/*
* Restituisce il nodo successivo a un determinato nodo della lista
*/
struct position * next(struct position *p);

/*
* Sostituisce l'elemento memorizzato nel nodo dato, restituendo il vecchio elemento
*/
int set(struct position *p, int e);

/*
* Inserisce un elemento in cima alla lista restutuendo la nuova posizione
*/
struct position * add_first(int e);

/*
* Inserisce un elemento in coda alla lista restutuendo la nuova posizione
*/
struct position * add_last(int e);

/*
* Inseriesce un elemento prima della data posizione
*/
void add_before(struct position *p, int e);

/*
* Inserisce un elemento dopo di una data posizione
*/
void add_after(struct position *p, int e);

/*
* Rimuove un elemento in una data posizione
*/
int remove_element(struct position *p);

/*
* Scorre tutti gli elementi della lista richiamando la callback per ogni elemento
*/
void ll_for_each(void (*callback)(struct position *p));

#endif
