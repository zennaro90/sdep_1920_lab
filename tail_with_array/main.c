#include <stdio.h>
#include <stdlib.h>

#include "tail.h"

// Utility function to add an element data in the stack 
// insert at the beginning 
void enqueue(int el) {
	tail[tailIdx] = el;
	tailIdx = (tailIdx + 1) % MAX_SIZE;
}

// Utility function to pop top  
// element from the stack 
int dequeue() {
	int t = tail[headIdx];
	headIdx = (headIdx + 1) % MAX_SIZE;
	return t;
}

// Utility function to return the stack size
int size() {
	return (MAX_SIZE - tailIdx + headIdx ) % MAX_SIZE;
}

// Utility function to check if the stack is empty or not 
int isEmpty() {
	return tailIdx == headIdx;
}

// Utility function to return top element in a stack 
int front() {
	return tail[headIdx];
}


int main(int argc, char *argv[]) {

	enqueue(1);
	enqueue(2);
	enqueue(3);
	enqueue(4);
	enqueue(5);
	

	int i = 0;
	while(!isEmpty()) {
		printf("Posizione %d: %d\n", i++, dequeue());
	}
	return 0;
}

