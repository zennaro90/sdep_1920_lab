
#define MAX_SIZE 100
static int tail[100];

static int headIdx = 0;
static int tailIdx = 0;


// Utility function to add an element data in the stack 
// insert at the beginning 
void enqueue(int el);

// Utility function to pop top  
// element from the stack 
int dequeue();

// Utility function to return the stack size
int size();

// Utility function to check if the stack is empty or not 
int isEmpty();

// Utility function to return top element in a stack 
int front();

