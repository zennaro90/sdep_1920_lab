#include <stdio.h>
#include <stdlib.h>

#include "stack.h"

// Utility function to add an element data in the stack 
// insert at the beginning 
void push(int el) {
 	struct Node *sEl = malloc(sizeof(struct Node));
 	sEl->next = HEAD;
	sEl->data = el;
	HEAD = sEl;
	SIZE++;
}

// Utility function to pop top  
// element from the stack 
int pop() {
	struct Node *top = HEAD;
	HEAD = top->next;
	int data = top->data;
	free(top);
	SIZE--;
	return data;
}

// Utility function to return the stack size
int size() {
	return SIZE;
}

// Utility function to check if the stack is empty or not 
int isEmpty() 
{ 
    return HEAD == NULL; 
} 

// Utility function to return top element in a stack 
int top() 
{ 
    // check for empty stack 
    if (!isEmpty()) 
        return HEAD->data; 
    else
        exit(1); 
} 

int main(int argc, char *argv[]) {

	push(1);
	push(2);
	push(3);
	push(4);
	push(5);
	

	int i = 0;
	while(!isEmpty()) {
		printf("Posizione %d: %d\n", i++, pop());
	}
	return 0;
}
