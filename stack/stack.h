struct Node {
	struct Node *next;
	int data;
};

static struct Node *HEAD;
static int SIZE = 0;


// Utility function to add an element data in the stack 
// insert at the beginning 
void push(int el);

// Utility function to pop top  
// element from the stack 
int pop();

// Utility function to return the stack size
int size();

// Utility function to check if the stack is empty or not 
int isEmpty();

// Utility function to return top element in a stack 
int top();
